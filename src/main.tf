resource "random_string" "terraform_auto" {
  upper   = false
  lower   = true
  special = false
  length  = 4
}
