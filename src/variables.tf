variable "project_id" {
  type        = string
  description = "GCP project ID"
  default     = "fita-interns"
}

variable "gcp_region" {
  type        = string
  description = "GCP region ID"
  default     = "asia-southeast2-a"
}
